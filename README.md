# ServsUp project


**ServsUp is an open-source, self-hosted status page application written in Node.js**

Use ServsUp to keep your customers informed about the status of your applications and services and communicate about downtime, or simply to keep an eye on your own internal services in real-time. Simply list the servers you want to check status for, and the application will automatically keep you visually updated. 

ServsUp can be setup anywhere, using Docker or Node directly in less than a minute.

Here is a screenshot of how it looks when running

![Node Server status application](http://servsup.co/app.png)

## Running ServsUp

### Using Docker

The easiest way to use the application is to use the Docker image. 

We assume you already have Docker setup. If not, have a look [here](https://docs.docker.com/engine/installation/).

To run the application, simply pull the image
```bash
$docker pull julienlengrand/servsup
```

and then run it. Don't forget to publish the port 80 to have access to the application.
```bash
$docker run -it -p 80:4567 julienlengrand/servsup
```

*Note : Also publish port 8080 if you want to have access to the rethinkdb web page.*

### From source

This assumes you have already installed :

* [Node and Npm](https://nodejs.org/en/download/)
* [Rethinkdb](https://www.rethinkdb.com/docs/install/)


* Pull the source code

```
$ git pull git@github.com:jlengrand/servsup.git
```

* Install all the project dependencies

```
$ npm install 
```

* Start Rethinkdb inside of the directory.

```
$ rethinkdb
```

* Now you can start the node server by running:

```
$ npm start
```

## ROADMAP

Non-exhaustive list of the ROADMAP contains : 

* Authentication
* Notifications (Email, text messages)
* Graphs and Outage history
* REST API endpoints

## LICENSE

This work is under the [CC-NC-ND License](https://www.tldrlegal.com/l/ccncnd).

## Authors

ServsUp is a project from **[Julien Lengrand-Lambert](http://www.lengrand.fr/)** and **[Ramon Gebben](https://www.linkedin.com/in/ramon-gebben-88039b85)**.