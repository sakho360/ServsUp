'use strict';

const r = require('rethinkdb');

var dbName = 'status_servers'; 
var tableName = 'servers'; 
var dbHost = 'localhost'; 
var dbPort = 28015; 

r.connect({
    host: dbHost,
    port: dbPort
}, function(err, conn) {
    // Creates DB
    r.dbCreate(dbName).run(conn, function(dbObject){
        r.connect({
                host: dbHost,
                port: dbPort,
                db: dbName
        }, function(err, conn) {
            // Creates table
            r.db(dbName).tableCreate(tableName).run(conn, function(tableObject){
                console.log("Done creating database!");
                process.exit()
            });
        });
    });
});