import React, { PropTypes } from 'react';
import Pencil from 'react-icons/lib/md/mode-edit';
import Delete from 'react-icons/lib/md/delete';

export default function Server(props) {
  const { serverName, status } = props;
  return (
    <div className={`panel ${status}`}>
      <h3 className="panel-header left">{serverName}</h3>
      <div className="actions right">
        <span className="badge left">{status}</span>
        <button
          type="button"
          className="action"
          onClick={props.onServerDelete.bind(null, props.id)}
        >
          <Delete />
        </button>
        <button
          type="button"
          className="action"
          onClick={props.onServerEdit.bind(null, props.id)}
        >
          <Pencil />
        </button>
      </div>
    </div>
  );
}

Server.propTypes = {
  id: PropTypes.string.isRequired,
  serverName: PropTypes.string.isRequired,
  serverUrl: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
};
