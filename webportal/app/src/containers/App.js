import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AddIcon from 'react-icons/lib/md/add';

import Server from '../components/Server';
import Modal from '../components/Modal';
import ServerForm from '../components/ServerForm';

import * as AppActions from '../actions/app';

import fetchServers from '../utils/fetchServers';
import getChangeType from '../utils/getChangeType';
import io from 'socket.io-client';


class App extends Component {

  constructor(props){
    super(props);
    this.props = props;
    this.state = {
      modal: {
        active: false,
        server: {},
      },
      servers: []
    };

    this.onModalClose = this.onModalClose.bind(this);
    this.onServerStatusChange = this.onServerStatusChange.bind(this);
    this.onServerAdd = this.onServerAdd.bind(this);
    this.onServerEdit = this.onServerEdit.bind(this);
    this.onServerSubmit = this.onServerSubmit.bind(this);
    this.onServerDelete = this.onServerDelete.bind(this);

    const socket = io(window.location.origin);
    socket.on('status_change', this.onServerStatusChange);
  }

  componentDidMount() {
    fetchServers()
      .then(servers => this.setState({ servers }));
  }

  onServerStatusChange(change) {
    const changeType = getChangeType(change);
    const changedServer = change.new_val || change.old_val;
    const servers = [].concat.apply(this.state.servers);
    const serverIndex = this.state.servers.findIndex(s => s.id === changedServer.id);

    switch (changeType) {
      case 'create':
        servers.push(changedServer);
        break;
      case 'delete':
        servers.splice(serverIndex, 1);
        break;
      default:
        servers[serverIndex] = changedServer;
        break;
    }

    this.setState({ servers });
  }

  onModalClose() {
    this.setState({ modal: { active: false, server: {} } });
  }

  onServerAdd() {
    this.setState({ modal: { active: true, server: {} } });
  }

  onServerEdit(serverId) {
    return () => {
      const server = this.state.servers.find(s => s.id === serverId);
      this.setState({ modal: { active: true, server } });
    };
  }

  onServerDelete(serverId) {
    return () => {
      fetch(window.location.origin + `/servers/${serverId}`, {
        method: 'DELETE',
      })
      .then(res => res.json())
      .then((res) => {console.log(res);});
    };
  }

  onServerSubmit(e, server) {
    if (!server.id) return this.createNewServer(server);
    return this.updateServer(server);
  }

  updateServer(server) {
    return fetch(window.location.origin + `/servers/${server.id}`, {
      method: 'PUT',
      body: JSON.stringify(server),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(this.onModalClose);
  }

  createNewServer(server) {
    return fetch(window.location.origin + '/servers', {
      method: 'POST',
      body: JSON.stringify(server),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(res => res.json())
    .then(this.onModalClose);
  }

  render() {
    const { servers, modal } = this.state;
    const { server } = modal;
    const { actions } = this.props;
    const modalActive = modal.active;

    return (
        <div className="app-container">
          <header>
            <a href="http://servsup.co/">
              <svg version="1.0" xmlns="http://www.w3.org/2000/svg"
               width="800.000000pt" height="600.000000pt" viewBox="0 0 800.000000 600.000000"
               preserveAspectRatio="xMidYMid meet">
                <g transform="translate(0.000000,600.000000) scale(0.100000,-0.100000)"
                fill="#f9f9f9" stroke="none">
                <path d="M3498 4080 c-100 -29 -170 -63 -276 -133 -153 -101 -402 -302 -426
                -344 -19 -34 13 -32 109 8 124 52 198 61 238 29 36 -28 63 -84 82 -163 14 -63
                20 -129 30 -383 15 -357 106 -601 304 -808 83 -86 144 -132 263 -195 157 -84
                437 -165 648 -187 93 -9 72 5 -112 80 -571 231 -850 518 -918 947 -5 37 -14
                161 -20 275 -13 276 -40 379 -127 475 -20 22 -33 43 -29 47 4 4 56 7 114 7 98
                0 111 -2 153 -27 102 -60 175 -176 219 -346 l23 -91 68 63 c73 67 179 127 269
                150 36 10 92 14 165 12 187 -6 326 -66 448 -195 57 -61 144 -188 134 -197 -2
                -2 -37 10 -78 27 -405 165 -788 49 -807 -246 -21 -335 346 -717 858 -893 110
                -38 246 -73 339 -87 l44 -7 -50 93 c-28 52 -103 198 -168 324 -65 127 -130
                253 -145 280 -15 28 -65 122 -110 210 -46 88 -87 166 -93 173 -5 7 -8 15 -5
                17 4 4 386 -131 481 -170 24 -10 22 8 -14 117 -145 439 -393 676 -755 721
                l-93 12 -63 85 c-120 162 -243 265 -374 311 -86 31 -237 35 -326 9z"/>
                </g>
              </svg>
              <h1>ServsUp.co</h1>
            </a>
          </header>
          {modalActive &&
            <Modal onModalClose={this.onModalClose}>
              <ServerForm
                server={server}
                onSubmit={this.onServerSubmit}
              />
            </Modal>
          }
          <div className="server-list">
            {servers.map(server => (
              <Server
                id={server.id}
                key={server.id}
                serverName={server.serverName}
                serverUrl={server.url}
                status={server.status}
                onServerEdit={this.onServerEdit(server.id)}
                onServerDelete={this.onServerDelete(server.id)}
              />
            ))}
            <button
              className="add-server btn"
              onClick={this.onServerAdd}
            >
              <AddIcon />
            </button>
          </div>
          <footer>
            <div className="inner">
            	<p>
            		<a href="https://gitlab.com/jlengrand/node-server-status">Source Code</a>
            		 &bull;
                  <a href="https://gitlab.com/jlengrand/node-server-status/issues/">Issues/Feature Requests </a>
            		 &bull;
                  <a href="https://www.tldrlegal.com/l/ccncnd">License </a>
              </p>
              <p>&copy; 2016 - Ramon Gebben &amp; Julien Lengrand-Lambert</p>
            </div>
          </footer>
        </div>
    );
  }
}

// Map state properties to this Component's properties.
const mapStateToProps = (state, props) => ({
  app: state.app,
});

// Bind Redux' `dispatch()` to this Component's actions.
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(Object.assign({}, AppActions), dispatch),
});

// Expose `state` and `dispatch()` to this Components properties and actions and
// bind them to this Component's instance.
export default connect(mapStateToProps, mapDispatchToProps)(App);
