

export default function fetchServers() {
  return fetch(window.location.origin + '/servers')
    .then(res => res.json());
}
